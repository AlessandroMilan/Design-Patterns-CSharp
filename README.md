![Build Status](https://gitlab.com/AlessandroMilan/Design-Patterns-CSharp/badges/master/build.svg)
# [Design-Patterns-CSharp](https://alessandromilan.gitlab.io/Design-Patterns-CSharp)
Design Patterns in C#.

## What is a Design Pattern
In software engineering, a design pattern is a general repeatable solution to a commonly occurring problem.
A design pattern is not a finished design that can be transformed directly into code. 
It is a description or template for how to solve a problem that can be used in many different situations.

### Types of Design Patterns
#### Creational patterns
- [Abstract Factory](/src/AbstractFactory/README.md)
- [Builder](/src/Builder/README.md)
- [Factory Method](/src/FactoryMethod/README.md)
- [Object Pool](/src/ObjectPool/README.md)
- [Prototype](/src/Prototype/README.md)
- [Singleton](/src/Singleton/README.md)
#### Structural patterns
- Adapter
- Bridge
- Composite
- Decorator
- Façade
- Flyweight
- Private Class Data
- Proxy
#### Behavioral patterns
- Chain of responsibility
- Command
- Interpreter
- Iterator
- Mediator
- Memento
- Null Object
- Observer
- State
- Strategy
- Template method
- Visitor


##### How to run test cases

###### Dependencies
* .NET CORE SDK >= 2.0

Clone this repository
```bash
git clone https://gitlab.com/AlessandroMilan/Design-Patterns-CSharp.git
```

Move to the solution
```bash
cd Design-Patterns-CSharp

```

Build and Run Test Cases!
```bash
dotnet build

dotnet test test/FactoryMethodTest/FactoryMethodTest.csproj
dotnet test test/AbstractFactoryTest/AbstractFactoryTest.csproj
dotnet test test/BuilderTest/BuilderTest.csproj
dotnet test test/PrototypeTest/PrototypeTest.csproj
```

---
This Repository was forked from [desarrolladorSLP/Design-Patterns-CSharp](https://github.com/desarrolladorSLP/Design-Patterns-CSharp)

Licensed under [MIT License](/LICENSE)