# Summary

* [Design-Patterns-CSharp](README.md)
* Types of Design Patterns
    * Creational patterns
        * [Abstract Factory](/src/AbstractFactory/README.md)
        * [Builder](/src/Builder/README.md)
        * [Factory Method](/src/FactoryMethod/README.md)
        * [Object Pool](/src/ObjectPool/README.md)
        * [Prototype](/src/Prototype/README.md)
        * [Singleton](/src/Singleton/README.md)
    * Structural patterns
        * Adapter
        * Bridge
        * Composite
        * Decorator
        * Façade
        * Flyweight
        * Private Class Data
        * Proxy
    * Behavioral patterns
        * Chain of responsibility
        * Command
        * Interpreter
        * Iterator
        * Mediator
        * Memento
        * Null Object
        * Observer
        * State
        * Strategy
        * Template method
        * Visitor
